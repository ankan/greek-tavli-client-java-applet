/*
 * PLEASE don't judge my coding ability from this piece of code.
 * I started learning Java just to write this client and
 * I hope I'll stop learning Java after that...
 * I don't like Java at all :-(
 *
 * Antonis Kanavouras
 */
import java.awt.*;
import java.applet.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.net.* ;
import java.io.*;
import java.util.* ;

public class tavli extends JApplet implements ActionListener, Runnable
{
    String VERSION = "1.5.0";
    //String server = "192.168.0.3";
    //String server = "127.0.0.1";
    String server = "www.exares.gr";
    int port = 2222 ;

    java.util.Timer tim;
    Tasky task ;

    boolean isApplet =true ;
    Socket sock ;
    boolean connected = false, onChan = false, player=false, turn=false,rolled=false,canmove=false ;
    boolean flash,stoprolling,sound=true ;
    boolean sendlock = false ;
    boolean given = false ;
    /*--- Game variables  & constatnts--*/
    String blackNick, whiteNick,oponick ;
    byte blackScor, whiteScor ;
    byte dice[] = {0,0,0,0};
    byte backupdice[] ={0,0,0,0};
    final byte BLACK = 1,WHITE = 2 ;
    final byte PORTES=0,PLAKWTO=1,FEYGA=2 ;
    String gN[] = {"Portes","Plakwto","Feyga"};
    byte playercolor = -1; // if not  player
    byte turncolor ;
    byte gameType =-1;
    byte loaded = -1 ;
    Puli ldp = null ;
    byte exmoves=0,exmovesback =0 ;
    byte donemoves=0 ;
    byte BHOME,WHOME;/*-- Map, not game logic --*/
    String moves[];
    String prevIRCString ;
    /*-- ---------- */

    String nick ;

    boolean updaterOnWork = false ; /*-- updating lists */
    BufferedReader in ;
    PrintWriter out;
    Thread NetReader ;
    JFrame frLogin;
    JMenuBar menu;
    JMenu connection,helpm,stateMenu,gameMenu,sndMenu;
    JMenuItem login,connect,disconnect;
    JMenuItem help, about ;
    JMenuItem free,silence,nameM ;
    JMenuItem part, resign, giveS, giveD, prive,quiet ;
    JRadioButtonMenuItem sndon,sndoff;
    JPopupMenu whoPop ;
    JMenu invite, point1 ;
    JMenuItem wmsg,invite3,invite5, invite7,portes, plakwto, feyga, cancelinv,wwhois,wignore;
    JPopupMenu gamesPop;
    JMenuItem watch;
    JPopupMenu namesPop;
    JMenuItem nmsg,nwhois,kick,ban,nignore;
    JPopupMenu boardPop ;
    JMenuItem undo ;
    JTextArea IRC;
    JTextField inIRC;
    Container container;
    JPanel glob;
    BoardPanel BP;
    JLabel labNick,labGame,labMoves ;
    JTabbedPane tabs ;
    JList who,names,games ;
    DefaultListModel whoV, namesV,gamesV, twhoV,tnamesV,tgamesV;
    ListUpdater  Lu ;
    Cursor hCur, nCur ;
    Zari[] yeldice,whidice ;
    Image blackNormal,blackHome,whiteNormal,whiteHome,test ;
    Roller roller ;
    Flasher flasher ;
    Map map[];
    AudioClip krr,ding,hurrah, tsaf[];

    final short vStep = 23 ;  /* VERTICAL STEP  Einai to ypsos tpu pouliou */
    final short hStep = 26 ; /* HORIZONTAL STEP IN THE SAME PANEL */
    final short olimit = 5 ; /* Ana posa poulia sto idio column ginontai stoives */
    final short hstep = 4; /* Overloaded poulia on a column > olimit */
    final short lpX = 42 ; /* LEFT PANELS X */
    final short rpX = 230; /* RIGHT PANELS X */
    final short tX = 200 ; /* TAKOS X */
    final short mX = 12 ; /* MAZEMA X */
    final short uY = 10 ;  /* UPPER PANELS Y */
    final short dY = 275 ; /* DOWN PANELS Y */
    final short pouliWidth = 23 ; /* POULI WIDTH & HEIGHT */

    public class Tasky extends TimerTask{
        public void run(){
            send("time limit exceeded. I must resign my friend...");
            send("/resign");
        }
    }

    public class MenuItemHandler implements ActionListener,ItemListener {
        public void itemStateChanged(ItemEvent ie){
            Object o = ie.getItem();
            if(o == sndon){
                sound =true ;
                return ;
            }
            if(o == sndoff){
                sound = false ;
                return ;
            }
        }

        public void actionPerformed (ActionEvent e) {
            Object o = e.getSource ();
            if (o == login){
                Point p =BP.getLocationOnScreen();
                showLogin ((int) p.getX(), (int) p.getY());
                return ;
            }
            if(o== connect){
                doConnection();
                return ;
            }
            if(o== disconnect){
                doDisconnection();
                return ;
            }
            if(o == wwhois){
                send("/whois "+ who.getSelectedValue());
                return ;
            }
            if(o == wignore){
                send("/ignore "+ who.getSelectedValue());
                return ;
            }
            if(o == wmsg){
                JFrame msgFr = new msgWin( (String) who.getSelectedValue());
                msgFr.setLocation(who.getLocationOnScreen());
                msgFr.setSize(300,300);
                msgFr.setResizable (false);
                msgFr.pack();
                msgFr.show();
                return ;
            }
            if(o == invite3){
                send("/inv " + who.getSelectedValue() + " 3");
                return ;
            }
            if(o == invite5){
                send("/inv " + who.getSelectedValue() + " 5");
                return ;
            }
            if(o == invite7){
                send("/inv " + who.getSelectedValue() + " 7");
                return ;
            }
            if(o == portes){
                send("/inv " + who.getSelectedValue() + " portes");
                return ;
            }
            if(o == plakwto){
                send("/inv " + who.getSelectedValue() + " plakwto");
                return ;
            }
            if(o == feyga){
                send("/inv " + who.getSelectedValue() + " feyga");
                return ;
            }
            if(o == cancelinv){
                send("/uninv");
                return ;
            }
            if(o == watch){
                StringTokenizer tok = new StringTokenizer( (String) games.getSelectedValue());
                send("/watch " + tok.nextToken());
                return ;
            }
            if(o == nwhois){
                send("/whois "+ names.getSelectedValue());
                return ;
            }
            if(o == nignore){
                send("/ignore "+ names.getSelectedValue());
                return ;
            }
            if(o == nmsg){
                JFrame msgFr = new msgWin( (String) names.getSelectedValue());
                msgFr.setLocation(names.getLocationOnScreen());
                msgFr.setSize(300,300);
                msgFr.setResizable (false);
                msgFr.pack();
                msgFr.show();
                return ;
            }
            if(o == kick){
                send("/kick "+ names.getSelectedValue());
                return ;
            }
            if(o == ban){
                send("/ban "+ names.getSelectedValue());
                return ;
            }
            if(o == free){
                send("/free");
                return ;
            }
            if(o == silence){
                send("/silence");
                return ;
            }
            if(o == nameM){
                Namer namer = new Namer();
                namer.start();
                return ;
            }
            if(o == part){
                send("/part");
                return ;
            }
            if(o == quiet){
                send("/quiet");
                return ;
            }
            if(o == resign){
                Resigner resigner = new Resigner();
                resigner.start();
                return ;
            }
            if(o == giveS && !given){
                Giver giver = new Giver((byte)1);
                giver.start();
		given = true ;
                return ;
            }
            if(o == giveD && !given){
                Giver giver = new Giver((byte)2);
                giver.start();
		given = true ;
                return ;
            }
            if(o == prive){
                Privater pr = new Privater();
                pr.start();
                return ;
            }

            if(o == about){
                Helper helper = new Helper((byte)0);
                helper.start();
                return ;
            }
            if(o == help){
                Helper helper = new Helper((byte)1);
                helper.start();
                return;
            }
            if(o == undo){
                turn = true ;
                loaded = -1 ;
                for(byte i =0;i<4;i++){
                    dice[i] = backupdice[i] ;
                }
                BP.restoreOrig();
                BP.cpOrigToTmp();
                donemoves = 0;
                exmoves = exmovesback ;
                BP.repaint();
                return;
            }

        }
    }

    /*-- The login dialog window --*/
    public class loginWin extends JFrame implements ActionListener  {
        JButton skip, ok;
        JPasswordField pass;
        JTextField nick;

        loginWin (String s)    {
            super (s);
            Container cont = this.getContentPane ();
            cont.setLayout (new BorderLayout ());
            pass = new JPasswordField (15);
            pass.addActionListener (this);
            nick = new JTextField (15);
            JLabel lnick = new JLabel ("nick: ");
            JLabel lpass = new JLabel ("pass:");
            ok = new JButton (" ok ");
            ok.addActionListener (this);
            skip = new JButton ("skip");
            skip.addActionListener (this);
            JPanel nPan = new JPanel ();
            JPanel cPan = new JPanel ();
            JPanel sPan = new JPanel ();
            nPan.add (lnick);
            nPan.add (nick);
            cPan.add (lpass);
            cPan.add (pass);
            sPan.add (ok);
            sPan.add (skip);
            cont.add (nPan, BorderLayout.NORTH);
            cont.add (cPan, BorderLayout.CENTER);
            cont.add (sPan, BorderLayout.SOUTH);
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }

        public void login (String nick, String pass){
            if( ! connected){
                toIRC("Not connected\n");
                connected = false ;
            }   else if(nick.length() == 0){
                return ;
            } else {
                send ("/user " + nick);
                send("/pass " + pass);
                connected = true ;
            }
        }
        public void actionPerformed (ActionEvent e) {
            Object o = e.getSource ();
            if (o == skip){
                try {
                    this.dispose ();
                }finally {
                    return;
                }
            }else{
                login (nick.getText (), new String (pass.getPassword ()));
                try{
                    this.dispose ();
                }finally {
                    return;
                }
            }
        }
    }

    public class msgWin extends JFrame implements ActionListener {
        JButton skip, ok;
        JTextField msg;
        String sm ;

        msgWin (String s)
        {
            super ("message to " + s);
            sm = s ;
            Container cont = this.getContentPane ();
            cont.setLayout (new BorderLayout ());
            msg = new JTextField (30);
            msg.addActionListener(this);
            ok = new JButton ("send");
            ok.addActionListener (this);
            skip = new JButton ("skip");
            skip.addActionListener (this);
            JPanel nPan = new JPanel ();
            JPanel sPan = new JPanel ();
            nPan.add (msg);
            sPan.add (ok);
            sPan.add (skip);
            cont.add (nPan, BorderLayout.NORTH);
            cont.add (sPan, BorderLayout.SOUTH);
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }
        public void actionPerformed (ActionEvent e)
        {
            Object o = e.getSource ();
            if (o == skip)
            {
                try
                {
                    this.dispose ();
                }
                finally
                {
                    return;
                }
            }
            else
            {
                String m = msg.getText();
                if(!connected || m.length() == 0){
                    ;
                }else{
                    send("/msg " + sm +" " + m);
                }
                try
                {
                    this.dispose ();
                }
                finally
                {
                    return;
                }
            }
        }
    }

    public class Column {
        byte bpop ;
        byte wpop ;
        byte top ;
        byte m ; /* O sxetikos index sto map[] (for drawing)*/
    }

    public class Map{
        int x=0 ;
        int y=0;
        int vstep=0 ;
        int alx=0 ;
        int arx=0 ;
        int auy=0 ;
        int ady=0 ;
        int col=-1 ;
    }

    public class Zari extends ImageIcon implements MouseListener,MouseMotionListener {
        boolean cursorblocked ;
        int x,y,width,height ;
        int area = 22 ;
        boolean ok = false ;
        Zari(Image i){
            super(i);
            width = this.getIconWidth();
            height = this.getIconHeight();
            addMouseListener(this);
            addMouseMotionListener(this);
        }
        Zari(String i){
            super(i);
            width = this.getIconWidth();
            height = this.getIconHeight();
            addMouseListener(this);
            addMouseMotionListener(this);
        }
        public void setX(int i){
            this.x = i ;
        }
        public void setY(int i){
            this.y = i ;
        }
        public void mouseClicked(MouseEvent e){}
        public void mouseEntered (MouseEvent e){}
        public void mouseExited (MouseEvent e){}
        public void mouseReleased (MouseEvent e){}
        public void mousePressed(MouseEvent e){
            if(!player || rolled || this == BP.z1){
                return ;
            }
            rolled = true ;
            roll();


        }
        public void mouseDragged(MouseEvent e){}
        public void mouseMoved(MouseEvent e){
            /*
             	if(this == BP.z1) return ;
             	if(!player || !turn|| rolled){
             		BP.setCursor(nCur);
             		ok = false ;
             		return ;
             	}
             
             	x = BP.z1x ;
             	y = BP.z1y ;
               int X = e.getX();
               int Y = e.getY();
               double absx = BP.getLocation().getX() + x;
               double absy =BP.getLocation().getY() + y;
                  if(X > (absx - area) && X < (absx + width + area) && Y> (absy - area ) && Y < (absy + height + area + 32)){
                  	  ok = true ;
                      BP.setCursor(hCur);
                   }  else {
                   		ok = false ;
                         BP.setCursor(nCur);
                   }
            */
        }

    }

    public class BoardIcon extends ImageIcon implements MouseListener {

        BoardIcon (Image i) {
            super (i);
            addMouseListener (this);
        }
        BoardIcon (String imageFilename){
            super (imageFilename);
            addMouseListener (this);
        }
        /*------------*/
        public void mousePressed (MouseEvent e){
            if(e.isPopupTrigger() && turn && rolled){
                boardPop.show(BP,e.getX(),e.getY());
                return ;
            }


            if(loaded <0 ||!turn || !rolled){
                return ;
            }

            int mX = e.getX();
            int mY = e.getY();
            int x =(int)( BP.getLocation().getX())   ;
            int y =(int)( BP.getLocation().getY()) + 20;
            x = mX -x;
            y = mY - y;
            byte c = -1 ;
            for (int i = 0;i<26;i++){
                if(x>map[i].alx && x< map[i].arx && y> map[i].auy && y< map[i].ady){
                    c = (byte) map[i].col ;
                    break ;
                }
            }

            if(c== -1 || loaded == c){
                loaded = -1;
                BP.setActive();
                BP.repaint();
                return ;
            }
            if(playercolor == WHITE && gameType == FEYGA && c == WHOME){
                c = 12 ;
            } /* Remapping 12 to WHOME for feyga */

            byte alma ;
            if(gameType == FEYGA){
                alma = (byte)(loaded - c);
                if(alma < -10 && playercolor == WHITE && (loaded <=6 && loaded >=1)) {
                    alma +=24 ;
                }
                if(playercolor == WHITE && c == 12){
                    c = 25 ;/*-- domove returns whome --*/
                }
            }else if(gameType == PORTES){
                if(ldp.color == BLACK){
                    alma =(byte)( c - loaded);
                } else {
                    alma =(byte)( loaded - c);
                }
            }else{
                if(ldp.color == WHITE){
                    alma =(byte)( c - loaded);
                } else {
                    alma = (byte)(loaded - c);
                }
            }

            if(alma < 0){ /* He tries to move backwards */
                loaded = -1;
                BP.setActive();
                BP.repaint();
                return ;
            }

            if(BP.trymove(BP.mycol, loaded, alma, ldp.color,  gameType) ==false){ /*-- Den ton dexetai ekei to meros --*/
                loaded = -1 ;
                BP.setActive();
                BP.repaint();
                return ;
            }

            int dest = -1;
            byte j = 0;
            String[] tmp = new String[4] ;
            int start = loaded ;

            for(byte i =0;i<4;i++){ /*-- Test an me polles zaries --*/
                if(dice[i] ==0){continue ;}
                if( (dest =BP.domove(BP.tmpcol, start, dice[i], ldp.color,  gameType) ) >=0){
                    tmp[j] =new String(" "+ start + "/" + dice[i]);
                    dice[i] = 0 ;
                    start = dest ;
                    j++ ;
                    if(dest == c){
                        byte r = (byte)((Math.random()) * tsaf.length) ;
                        lala(tsaf[r],false);

                        for(byte k = 0 ;k<j;k++){
                            moves[donemoves++] = tmp[k] ;
                        }

                        if(donemoves == exmoves){
                            turn =false ;
                            undo.setEnabled(false);
                            sendMove();
                            try{ tim.cancel(); tim=null;}catch(Exception le){}
                            donemoves = 0;
                        }

                        BP.cpTmpToOrig();
                        loaded = -1;
                        BP.setActive();
                        BP.repaint();
                        return ;
                    }
                }
            }


            for(byte i =0; i<4;i++){
                dice[i] = backupdice[i] ;
            }


            j = 0 ;
            dest = -1 ;
            start = loaded ;
            BP.cpOrigToTmp();
            for(byte i =3;i >=0;i--){ /*-- Anapoda --*/
                if(dice[i] ==0){continue ;}
                if( (dest =BP.domove(BP.tmpcol, start, dice[i], ldp.color,  gameType) ) >=0){
                    tmp[j] =new String(" "+ start + "/" + dice[i]);
                    dice[i] = 0 ;
                    start = dest ;
                    j++ ;
                    if(dest == c){

                        byte r = (byte)((Math.random()) * tsaf.length) ;
                        lala(tsaf[r],false);

                        for(byte k = 0 ;k<j;k++){
                            moves[donemoves++] = tmp[k] ;
                        }

                        if(donemoves == exmoves){
                            turn = false ;
                            undo.setEnabled(false);
                            sendMove();
                            try{ tim.cancel(); tim = null ;}catch(Exception le){}
                            donemoves = 0;
                        }

                        BP.cpTmpToOrig();
                        loaded = -1;
                        BP.setActive();
                        BP.repaint();
                        return ;
                    }
                }
            }

            for(byte i =0; i<4;i++){
                dice[i] = backupdice[i] ;
            }

            loaded = -1;
            BP.setActive();
            BP.repaint();
            BP.cpOrigToTmp();
        }

        public void mouseClicked (MouseEvent e){}
        public void mouseEntered (MouseEvent e){}
        public void mouseExited (MouseEvent e){}
        public void mouseReleased (MouseEvent e){
            if(e.isPopupTrigger() && turn && rolled){
                boardPop.show(BP,e.getX(),e.getY());
                return ;
            }
        }
    }

    public class Puli extends ImageIcon implements MouseListener{
        int x,y;
        int width,height ;
        double absx,absy ;
        boolean cursorblocked = true ;
        byte color ;
        byte col ;

        Puli (Image i,byte cl ){
            super(i);
            addMouseListener(this);
            height = this.getIconHeight();
            width=this.getIconWidth();
            this.color = cl ;

        }
        Puli (String  imgFilename,byte cl ){
            super(imgFilename);
            addMouseListener(this);
            height = this.getIconHeight();
            width=this.getIconWidth();
            this.color = cl ;

        }


        public void mousePressed (MouseEvent e){

            if( this.color != playercolor ||  !turn || !rolled  || !canmove || loaded >0){
                return ;
            }

            int X = e.getX();
            int Y = e.getY();
            absx =( BP.getLocation().getX()) + x;
            absy =( BP.getLocation().getY()) + y;

            if(X > this.absx && X < (this.absx + this.getIconWidth()) && Y> this.absy + 20 && Y < this.absy +20 + this.getIconHeight()){
                if (BP.checkIfcanMove(this) == false){
                    loaded = -1 ;
                    BP.setActive();
                    BP.repaint();
                    return ;
                }
                loaded = this.col ;
                ldp = this ;
                BP.setActive(this.color == BLACK?BLACK:WHITE,this.x,this.y);
                BP.repaint();
            }
        }
        public void mouseClicked (MouseEvent e){}
        public void mouseEntered (MouseEvent e){}
        public void mouseExited (MouseEvent e){}
        public void mouseReleased (MouseEvent e){}

    }



    /*-- BoardPanel EDW GINETAI OLH H DOYLEIA */
    public class BoardPanel extends JPanel  {

        private byte blackTak,whiteTak,blackTakMap,whiteTakMap ;
        private BoardIcon imgBoard;
        public ImageIcon active ;
        int acX,acY;
        Zari z1 ,z2 ;
        int z1x,z2x,z1y,z2y ;
        private boolean lock ;
    private Column mycol[], backupcol[],tmpcol[] ;
        private Puli blackArr[];
        private Puli whiteArr[];
        private  ImageIcon Blm,Whm, Bac, Wac ;

        public BoardPanel (){
            if(isApplet){
                imgBoard = new BoardIcon(getImage(getDocumentBase(), "images/board-1.gif"));
            } else {
                imgBoard = new BoardIcon("images/board-1.gif");
            }

            mycol = new Column[26] ;
            for(int i =0;i<26;i++){
                mycol[i] = new Column();
            }
            backupcol = new Column[26] ;
            for(int i =0;i<26;i++){
                backupcol[i] = new Column();
            }
            tmpcol = new Column[26] ;
            for(int i =0;i<26;i++){
                tmpcol[i] = new Column();
            }

            /*-- Load poulia --*/
            blackArr = new Puli[15];
            whiteArr = new Puli[15];

            if(isApplet){
                for(int i =0;i<15;i++){
                    blackArr[i] = new Puli( getImage(getDocumentBase(),"images/blackpul.gif"),BLACK);
                }
                for(int i =0;i<15;i++){
                    whiteArr[i] = new Puli( getImage(getDocumentBase(),"images/whitepul.gif"),WHITE);
                }
                Blm = new ImageIcon( getImage(getDocumentBase(),"images/blackhome.gif"));
                Whm = new ImageIcon( getImage(getDocumentBase(),"images/whitehome.gif"));
                Bac = new ImageIcon( getImage(getDocumentBase(),"images/blackactive.gif"));
                Wac = new ImageIcon( getImage(getDocumentBase(),"images/whiteactive.gif"));
            }  else {
                for(int i =0;i<15;i++){
                    blackArr[i] = new Puli("images/blackpul.gif",BLACK);
                }
                for(int i =0;i<15;i++){
                    whiteArr[i] = new Puli("images/whitepul.gif",WHITE);
                }
                Blm = new ImageIcon( "images/blackhome.gif");
                Whm = new ImageIcon( "images/whitehome.gif");
                Bac = new ImageIcon( "images/blackactive.gif");
                Wac = new ImageIcon("images/whiteactive.gif");
            }
            /*-- end loading poulia --*/

        }

        public synchronized  void paintComponent (Graphics g) {
            while (lock) {
                try {
                    wait();
                } catch (InterruptedException e) { }
            }
            lock = true;

        super.paintComponent (g);
            imgBoard.paintIcon (null, g, 0, 0);
            int w=0,b=0 ,bp, wp;

            for(byte i = 0 ;i<26;i++){
                if(i == WHOME){
                    int j = 0 ;
                    bp = mycol[i].bpop ;
                    while(bp-- >0){
                        blackArr[b].x= (map[blackTakMap].x);
                        blackArr[b].y =(map[blackTakMap].y +(j * map[blackTakMap].vstep));
                        blackArr[b].col = i;
                        blackArr[b].paintIcon(null,g,blackArr[b].x,blackArr[b].y);
                        b++ ; j++ ;
                    }
                    j=0;
                    wp = mycol[i].wpop ;
                    while(wp-- >0){
                        Whm.paintIcon(null,g,map[mycol[i].m].x,map[mycol[i].m].y + (j * map[mycol[i].m].vstep));
                        j++ ;
                    }
                } else if (i == BHOME){
                    int j = 0 ;
                    wp = mycol[i].wpop ;
                    while(wp-- >0){
                        whiteArr[w].x=(map[whiteTakMap].x);
                        whiteArr[w].y=(map[whiteTakMap].y +(j * map[whiteTakMap].vstep));
                        whiteArr[w].col = i;
                        whiteArr[w].paintIcon(null,g,whiteArr[w].x,whiteArr[w].y);
                        w++ ; j++ ;
                    }
                    j=0;
                    bp = mycol[i].bpop ;
                    while(bp-- >0){
                        Blm.paintIcon(null,g,map[mycol[i].m].x,map[mycol[i].m].y + (j * map[mycol[i].m].vstep));
                        j++ ;
                    }
                } else {
                    int j =0 , h = 0;
                    if(mycol[i].bpop >0 && mycol[i].top != BLACK){ /* Piasmeno mayro sto plakwto */
                        blackArr[b].x = (map[mycol[i].m].x);
                        blackArr[b].y = (map[mycol[i].m].y);
                        blackArr[b].col = i;
                        blackArr[b].paintIcon(null,g,blackArr[b].x,blackArr[b].y);
                        b++ ;
                        j =1 ;
                        wp = mycol[i].wpop ;
                        while( wp-- >0){
                            whiteArr[w].x =(map[mycol[i].m].x + (hstep * h));
                            whiteArr[w].y =(map[mycol[i].m].y + (map[mycol[i].m].vstep * j));
                            whiteArr[w].col = i;
                            whiteArr[w].paintIcon(null,g,whiteArr[w].x,whiteArr[w].y);
                            j++;
                            if(j==olimit){
                                j=0;
                                h++ ;
                            }
                            w++ ;
                        }
                        continue ;
                    }
                    h =0 ; j = 0 ;
                    if(mycol[i].wpop >0 && mycol[i].top != WHITE){ /* Piasmeno aspro sto plakwto */
                        whiteArr[w].x=(map[mycol[i].m].x);
                        whiteArr[w].y=(map[mycol[i].m].y);
                        whiteArr[w].col = i;
                        whiteArr[w].paintIcon(null,g,whiteArr[w].x,whiteArr[w].y);
                        w++ ;
                        j =1 ;
                        bp = mycol[i].bpop ;
                        while( bp-- >0){
                            blackArr[b].x=(map[mycol[i].m].x + (hstep * h));
                            blackArr[b].y=(map[mycol[i].m].y + (map[mycol[i].m].vstep * j));
                            blackArr[b].col = i;
                            blackArr[b].paintIcon(null,g,blackArr[b].x,blackArr[b].y);
                            j++;
                            if(j== olimit){
                                j=0;
                                h++ ;
                            }
                            b++ ;
                        }
                        continue ;
                    }
                    h =0 ; j =0;
                    bp =mycol[i].bpop ;
                    while( bp-- >0){
                        blackArr[b].x=(map[mycol[i].m].x + (hstep * h));
                        blackArr[b].y=(map[mycol[i].m].y + (map[mycol[i].m].vstep * j));
                        blackArr[b].col = i;
                        blackArr[b].paintIcon(null,g,blackArr[b].x,blackArr[b].y);
                        j++;
                        if(j== olimit ){
                            j=0;
                            h++ ;
                        }
                        b++ ;
                    }
                    wp = mycol[i].wpop ;
                    while( wp-- >0){
                        whiteArr[w].x=(map[mycol[i].m].x + (hstep * h));
                        whiteArr[w].y=(map[mycol[i].m].y + (map[mycol[i].m].vstep * j));
                        whiteArr[w].col = i;
                        whiteArr[w].paintIcon(null,g,whiteArr[w].x,whiteArr[w].y);
                        j++;
                        if(j== olimit ){
                            j=0;
                            h++ ;
                        }
                        w++ ;
                    }
                } //if
            }  // for
            if(z1 !=null && z2 != null){
                z1.paintIcon(null, g, z1x, z1y);
                z2.paintIcon(null, g, z2x , z2y);
            }

            if(this.active != null){
                this.active.paintIcon(null,g,acX,acY); /* An se ena pouli  ginetai click -> highlight */
            }
            lock = false ;
        notifyAll();
        } // end function

        public Dimension getPreferredSize () {
            return new Dimension (imgBoard.getIconWidth(), imgBoard.getIconHeight());
        }

        public void setActive(byte cl,int x, int y){
            if(cl == BLACK){
                this.active = Bac;
            }else{
                this.active = Wac;
            }
            acX = x ;
            acY = y ;
        }

        public void setActive(){
            this.active = null ;
        }

        public synchronized  void initGame(byte g,byte cl){
            while (lock) {
                try {
                    wait();
                } catch (InterruptedException e) { }
            }
            lock = true;

        for(byte i=0 ; i<26;i++){
                mycol[i].bpop = 0;
                mycol[i].wpop = 0 ;
                mycol[i].top = 0;
            }

            if (g == PORTES){
                mycol[1].bpop = 2;
                mycol[1].top = BLACK;
                mycol[6].wpop = 5;
                mycol[6].top = WHITE;
                mycol[8].wpop = 3;
                mycol[8].top = WHITE;
                mycol[12].bpop = 5;
                mycol[12].top = BLACK;
                mycol[13].wpop = 5;
                mycol[13].top = WHITE;
                mycol[17].bpop = 3;
                mycol[17].top = BLACK;
                mycol[19].bpop = 5;
                mycol[19].top = BLACK;
                mycol[24].wpop = 2;
                mycol[24].top = WHITE;
                BHOME = 25;
                WHOME = 0;

            }else if(g == PLAKWTO){
                mycol[1].wpop = 15;
                mycol[1].top = WHITE;
                mycol[24].bpop = 15;
                mycol[24].top = BLACK;
                BHOME = 0;
                WHOME = 25;

            }else if (g == FEYGA){
                mycol[12].wpop = 15;
                mycol[12].top = WHITE;
                mycol[24].bpop = 15;
                mycol[24].top = BLACK;
                BHOME = 0;
                WHOME = 25;

            }else if (g== -2){
                for(byte i=0;i<26;i++){
                    mycol[i].bpop = 0;
                    mycol[i].wpop=0;
                }
            }else {
                mycol[0].bpop =15;
                mycol[25].wpop = 15 ;
                BHOME = 0;
                WHOME = 25;
            }
            equcols();
            BP.setActive(); /*-- To avoid ghost active puli --*/
            lock = false ;
        notifyAll();
        }

        public synchronized  void createBoardMap(boolean pl,byte cl){
            /* Kanei asign map[] members se col[] members wste na
             * strwnetai to tavli swsta gia ka8e paikth kai oxi na paizei anapoda
             * o aspros opws ginetai se ASCII mode */

            while (lock) {
                try {
                    wait();
                } catch (InterruptedException e) { }
            }
            lock = true;

        if(pl && cl == WHITE){
                if(gameType == FEYGA){
                    for(byte i = 1 ; i<13 ; i++){
                        mycol[i].m =(byte) (i + 12) ;
                        map[i+12].col = i ;
                    }
                    for(byte i =13 ; i<25 ; i++){
                        mycol[i].m = (byte)(i - 12) ;
                        map[i-12].col = i ;
                    }
                } else {
                    for(byte i = 0;i<26;i++){
                        mycol[i].m = (byte) (25 - i) ;
                        map[25 -i].col = i ;
                    }
                }
                blackTak =25 ;
                blackTakMap =  26 ;
                whiteTak = 0 ;
                whiteTakMap = 27 ;

            }else{

                for (byte i =0;i<26;i++){
                    mycol[i].m = i ;
                    map[i].col = i;
                }
                blackTak =0 ;
                blackTakMap =  27 ;
                whiteTak = 25 ;
                whiteTakMap = 26 ;
            }

            lock = false ;
        notifyAll();
        }

        public boolean checkIfcanMove(Puli p){
            Column[] brd = BP.mycol ;
            for(byte i=0;i<4;i++){
                if(dice[i]<1 || dice[i]>6){
                    continue ;
                }
                if(trymove(brd, p.col,dice[i],p.color,gameType) == true){
                    return true ;
                }
            }
            return false ;
        }

        public boolean trymove(Column[] board, int start, byte dice,byte color, byte game){
            int bhome, whome, blim, wlim, i, dest;
            if (board[start].top != color) {
                return (false);
            }
            switch (game) {
            case 0:{  /* PORTES */
                    bhome = 25;
                    whome = 0;
                    blim = 19;
                    wlim = 6;
                    switch (color) {
                    case 1:{ /* BLACK */
                            int bmin = 0;
                            dest = start + dice;
                            for (i = 24; i > 0; i--) {
                                if (board[i].top == BLACK) {
                                    bmin = i;
                                }
                            }
                            if (start == bmin && dest > bhome) {
                                dest = bhome;
                            }

                            /*-- mazema. Valid or not?-----------*/
                            if (dest == bhome && bmin < blim) {
                                return (false);
                            }
                            if (dest > bhome) {
                                return (false);
                            }

                            /*-- If poulia on takos can't play --*/
                            if (board[whome].bpop > 0 && start != whome) {
                                return (false);
                            }

                            /*-- Valid mazema. No more checks --*/
                            if (dest == bhome) {
                                return (true);
                            }

                            if (board[dest].wpop < 2) {
                                return (true);
                            }
                            return (false);
                        }
                    case 2:{ /* WHITE */
                            int wmax = 0;
                            dice *=(byte) -1;
                            /*-- White moves backwards in portes --*/
                            dest = start + dice;
                            for (i = 0; i <= 24; i++) {
                                if (board[i].top == WHITE) {
                                    wmax = i;
                                }
                            }
                            if (start == wmax && dest < whome) {
                                dest = whome;
                            }

                            /*-- mazema. Valid or not?-----------*/
                            if (dest == whome && wmax > wlim) {
                                return (false);
                            }
                            if (dest < whome) {
                                return (false);
                            }

                            /*-- If poulia on takos can't play --*/
                            if (board[bhome].wpop > 0 && start != bhome) {
                                return (false);
                            }

                            /*-- Valid mazema. No more checks --*/
                            if (dest == whome) {
                                return (true);
                            }

                            if (board[dest].bpop < 2) {
                                return (true);
                            }
                        }
                    }
                    return (false);
                }
            case 1:{ /* PLAKWTO */
                    bhome = 0;
                    whome = 25;
                    blim = 6;
                    wlim = 19;
                    switch (color) {
                    case 1:{ /* BLACK */
                            int bmax = 0;
                            int blocked = 0;
                            dice *=(byte) -1;
                            /*-- black moves backwards in plakwto --*/
                            dest = start + dice;
                            for (i = 1; i <= 24; i++) {
                                if (board[i].top == BLACK) {
                                    bmax = i;
                                } else {
                                    if (board[i].bpop > 0) {
                                        blocked++;
                                    }
                                }
                            }
                            if (start == bmax && dest < bhome) {
                                dest = bhome;
                            }
                            if (dest == bhome && (blocked > 0 || bmax > blim)) {
                                return (false);
                            }
                            if (dest < bhome) {
                                return (false);
                            }
                            if (board[dest].top == WHITE && (board[dest].wpop > 1 || board[dest].bpop > 0)) {
                                return (false);
                            }
                            return (true);
                        }
                    case 2:{ /* WHITE */
                            int wmin = 0;
                            int blocked = 0;
                            dest = start + dice;
                            for (i = 24; i > 0; i--) {
                                if (board[i].top == WHITE) {
                                    wmin = i;
                                } else {
                                    if (board[i].wpop > 0) {
                                        blocked++;
                                    }
                                }
                            }
                            if (start == wmin && dest > whome) {
                                dest = whome;
                            }
                            if (dest == whome && (blocked > 0 || wmin < wlim)) {
                                return (false);
                            }
                            if (dest > whome) {
                                return (false);
                            }
                            if (board[dest].top == BLACK && (board[dest].bpop > 1 || board[dest].wpop > 0)) {
                                return (false);
                            }
                            return (true);
                        }
                    }
                    return (false);
                }
            case 2:{ /* FEYGA */
                    bhome = 0;
                    whome = 12;
                    /*-- only for this function --*/
                    blim = 6;
                    dice *=(byte) -1;/*-- Everybody moves backwards --*/
                    switch (color) {
                    case 1:{ /* BLACK */
                            int bmax = 0;
                            int bmin = 0;
                            dest = start + dice;
                            for (i = 1; i <= 24; i++) {
                                if (board[i].top == BLACK) {
                                    bmax = i;
                                }
                            }
                            for (i = 24; i > 0; i--) {
                                if (board[i].top == BLACK) {
                                    bmin = i;
                                }
                            }
                            if (start == 24 && board[24].bpop == 14 && bmin > 12) {
                                return (false);
                            }
                            if (start == bmax && dest < bhome) {
                                dest = bhome;
                            }
                            if (dest == bhome && bmax > blim) {
                                return (false);
                            }
                            if (dest < bhome) {
                                return (false);
                            }
                            if (board[dest].top == WHITE) {
                                return (false);
                            }
                            return (true);
                        }
                    case 2:{ /* WHITE */
                            byte wlimup = 18;
                            byte wlimd = 13;
                            int wmin = 0;
                            int wmax = 0;
                            dest = start + dice;
                            dest = dest < 1 ? (dest + 24) : dest;
                            for (i = 24; i > 0; i--) {
                                if (board[i].top == WHITE) {
                                    wmin = i;
                                }
                            }
                            for (i = 1; i <= 24; i++) {
                                if (board[i].top == WHITE) {
                                    wmax = i;
                                }
                            }
                            if (start == 12 && board[12].wpop == 14 && wmax < 13) {
                                return (false);
                            }
                            if (wmin >= wlimd && wmax <= wlimup && dest <= whome) {
                                if (start == wmax) {
                                    dest = whome;
                                }
                                if (dest < whome) {
                                    return (false);
                                }
                            } else if (start > whome && dest <= whome) {
                                return (false);
                            }
                            if (dest == whome && (wmin < wlimd || wmax > wlimup)) {
                                return (false);
                            }

                            dest = dest == whome ? 25 : dest;

                            if (board[dest].top == BLACK) {
                                return (false);
                            }
                            return (true);
                        }
                    }
                    return (false);
                }
            }
            return (false);
        }

        /*-- Very similar to trymove() --*/
        public int domove(Column[] board, int start, byte dice,byte color, byte game){
            int bhome, whome, blim, wlim, i, dest;
            if (board[start].top != color) {
                return (-1);
            }
            switch (game) {
            case 0:{  /* PORTES */
                    bhome = 25;
                    whome = 0;
                    blim = 19;
                    wlim = 6;
                    switch (color) {
                    case 1:{ /* BLACK */
                            int bmin = 0;
                            dest = start + dice;
                            for (i = 24; i > 0; i--) {
                                if (board[i].top == BLACK) {
                                    bmin = i;
                                }
                            }
                            if (start == bmin && dest > bhome) {
                                dest = bhome;
                            }

                            /*-- mazema. Valid or not?-----------*/
                            if (dest == bhome && bmin < blim) {
                                return (-1);
                            }
                            if (dest > bhome) {
                                return (-1);
                            }

                            /*-- If poulia on takos can't play --*/
                            if (board[whome].bpop > 0 && start != whome) {
                                return (-1);
                            }

                            /*-- Valid mazema. No more checks --*/
                            if (dest == bhome) {
                                if (--board[start].bpop == 0) {
                                    board[start].top = 0;
                                }
                                board[dest].bpop++;
                                return (dest);
                            }

                            if (board[dest].wpop < 2) {
                                if (board[dest].top == WHITE) {
                                    board[bhome].wpop++;
                                    board[dest].wpop--;
                                    board[bhome].top = WHITE;
                                }
                                if (--board[start].bpop == 0) {
                                    board[start].top = 0;
                                }
                                board[dest].bpop++;
                                if (dest != bhome) {

                                    /*-- never set top at home. May be there
                                     *-- is opponent takos there and it nust be
                                                          *-- computed by cmtoex() --*/
                                    board[dest].top = BLACK;
                                }
                                return (dest);
                            }
                            return (-1);
                        }
                    case 2:{ /* WHITE */
                            int wmax = 0;
                            dice *=(byte) -1;
                            /*-- White moves backwards in portes --*/
                            dest = start + dice;
                            for (i = 0; i <= 24; i++) {
                                if (board[i].top == WHITE) {
                                    wmax = i;
                                }
                            }
                            if (start == wmax && dest < whome) {
                                dest = whome;
                            }

                            /*-- mazema. Valid or not?-----------*/
                            if (dest == whome && wmax > wlim) {
                                return (-1);
                            }
                            if (dest < whome) {
                                return (-1);
                            }
                            /*-- If poulia on takos can't play --*/
                            if (board[bhome].wpop > 0 && start != bhome) {
                                return (-1);
                            }

                            /*-- Valid mazema. No more checks --*/
                            if (dest == whome) {
                                if (--board[start].wpop == 0) {
                                    board[start].top = 0;
                                }
                                board[dest].wpop++;
                                return (dest);
                            }

                            if (board[dest].bpop < 2) {
                                if (board[dest].top == BLACK) {
                                    board[whome].bpop++;
                                    board[dest].bpop--;
                                    board[whome].top = BLACK;
                                }
                                if (--board[start].wpop == 0) {
                                    board[start].top = 0;
                                }
                                board[dest].wpop++;
                                if (dest != whome) {
                                    board[dest].top = WHITE;
                                }
                                return (dest);
                            }
                        }
                    }
                    return (-1);
                }
            case 1:{ /* PLAKWTO */
                    bhome = 0;
                    whome = 25;
                    blim = 6;
                    wlim = 19;
                    switch (color) {
                    case 1:{ /* BLACK */
                            int bmax = 0;
                            int blocked = 0;
                            dice *=(byte) -1;
                            /*-- black moves backwards in plakwto --*/
                            dest = start + dice;
                            for (i = 1; i <= 24; i++) {
                                if (board[i].top == BLACK) {
                                    bmax = i;
                                } else {
                                    if (board[i].bpop > 0) {
                                        blocked++;
                                    }
                                }
                            }
                            if (start == bmax && dest < bhome) {
                                dest = bhome;
                            }
                            if (dest == bhome && (blocked > 0 || bmax > blim)) {
                                return (-1);
                            }
                            if (dest < bhome) {
                                return (-1);
                            }
                            if (board[dest].top == WHITE && (board[dest].wpop > 1 || board[dest].bpop > 0)) {
                                return (-1);
                            }
                            if (--board[start].bpop == 0) {
                                if (board[start].wpop == 0) {
                                    board[start].top = 0;
                                } else {
                                    board[start].top = WHITE;
                                }
                            }
                            board[dest].bpop++;
                            board[dest].top = BLACK;
                            return (dest);
                        }
                    case 2:{ /* WHITE */
                            int wmin = 0;
                            int blocked = 0;
                            dest = start + dice;
                            for (i = 24; i > 0; i--) {
                                if (board[i].top == WHITE) {
                                    wmin = i;
                                } else {
                                    if (board[i].wpop > 0) {
                                        blocked++;
                                    }
                                }
                            }
                            if (start == wmin && dest > whome) {
                                dest = whome;
                            }
                            if (dest == whome && (blocked > 0 || wmin < wlim)) {
                                return (-1);
                            }
                            if (dest > whome) {
                                return (-1);
                            }
                            if (board[dest].top == BLACK && (board[dest].bpop > 1 || board[dest].wpop > 0)) {
                                return (-1);
                            }
                            if (--board[start].wpop == 0) {
                                if (board[start].bpop == 0) {
                                    board[start].top = 0;
                                } else {
                                    board[start].top = BLACK;
                                }
                            }
                            board[dest].wpop++;
                            board[dest].top = WHITE;
                            return (dest);
                        }
                    }
                    return (-1);
                }
            case 2:{ /* FEYGA */
                    bhome = 0;
                    whome = 12;
                    /*-- only for this function --*/
                    blim = 6;
                    dice *=(byte) -1;/*-- Everybody moves backwards --*/
                    switch (color) {
                    case 1:{ /* BLACK */
                            int bmax = 0;
                            int bmin = 0;
                            dest = start + dice;
                            for (i = 1; i <= 24; i++) {
                                if (board[i].top == BLACK) {
                                    bmax = i;
                                }
                            }
                            for (i = 24; i > 0; i--) {
                                if (board[i].top == BLACK) {
                                    bmin = i;
                                }
                            }
                            if (start == 24 && board[24].bpop == 14 && bmin > 12) {
                                return (-1);
                            }
                            if (start == bmax && dest < bhome) {
                                dest = bhome;
                            }
                            if (dest == bhome && bmax > blim) {
                                return (-1);
                            }
                            if (dest < bhome) {
                                return (-1);
                            }
                            if (board[dest].top == WHITE) {
                                return (-1);
                            }
                            if (--board[start].bpop == 0) {
                                board[start].top = 0;
                            }
                            board[dest].bpop++;
                            board[dest].top = BLACK;
                            return (dest);
                        }
                    case 2:{ /* WHITE */
                            byte wlimup = 18;
                            byte wlimd = 13;
                            int wmin = 0;
                            int wmax = 0;
                            dest = start + dice;
                            dest = dest < 1 ? (dest + 24) : dest;
                            for (i = 24; i > 0; i--) {
                                if (board[i].top == WHITE) {
                                    wmin = i;
                                }
                            }
                            for (i = 1; i <= 24; i++) {
                                if (board[i].top == WHITE) {
                                    wmax = i;
                                }
                            }
                            if (start == 12 && board[12].wpop == 14 && wmax < 13) {
                                return (-1);
                            }
                            if (wmin >= wlimd && wmax <= wlimup && dest <= whome) {
                                if (start == wmax) {
                                    dest = whome;
                                }
                                if (dest < whome) {
                                    return (-1);
                                }
                            } else if (start > whome && dest <= whome) {
                                return (-1);
                            }
                            if (dest == whome && (wmin < wlimd || wmax > wlimup)) {
                                return (-1);
                            }

                            dest = dest == whome ? 25 : dest;

                            if (board[dest].top == BLACK) {
                                return (-1);
                            }
                            if (--board[start].wpop == 0) {
                                board[start].top = 0;
                            }
                            board[dest].wpop++;
                            board[dest].top = WHITE;
                            return (dest);
                        }
                    }
                    return (-1);
                }
            }
            return (-1);
        }

        public  void equcols(){
            for(int i=0;i<26;i++){
                backupcol[i].bpop = tmpcol[i].bpop = mycol[i].bpop ;
                backupcol[i].wpop = tmpcol[i].wpop = mycol[i].wpop ;
                backupcol[i].top = tmpcol[i].top = mycol[i].top ;
                backupcol[i].m = tmpcol[i].m = mycol[i].m ;
            }
        }
        public  void cpOrigToTmp(){
            for(int i=0;i<26;i++){
                tmpcol[i].bpop = mycol[i].bpop ;
                tmpcol[i].wpop = mycol[i].wpop ;
                tmpcol[i].top = mycol[i].top ;
                tmpcol[i].m = mycol[i].m ;
            }

        }
        public  void cpTmpToOrig(){
            for(int i=0;i<26;i++){
                mycol[i].bpop = tmpcol[i].bpop ;
                mycol[i].wpop = tmpcol[i].wpop ;
                mycol[i].top = tmpcol[i].top ;
                mycol[i].m = tmpcol[i].m ;
            }
        }
        public  void restoreOrig(){
            for(int i=0;i<26;i++){
                mycol[i].bpop = backupcol[i].bpop ;
                mycol[i].wpop = backupcol[i].wpop ;
                mycol[i].top = backupcol[i].top ;
                mycol[i].m = backupcol[i].m ;
            }
        }

        public  void setZaria(boolean b){
            if(!b){
                z1y = z2y =-20 ;
                z1 = null ;
                z2 =null ;
                return;
            }

            if(dice[0] >0 && dice[1]>0){
                z1 = whidice[dice[0] -1];
                z2 = whidice[dice[1] -1];
            } else {
                z1 = whidice[(byte)Math.random() * 6];
                z2 = whidice[(byte)Math.random() * 6];
            }
            z1y = 150 ; z2y =150 ;
            if(turn){z1x =284  ;}else{z1x = 109;}
            z2x = z1x +19 ;
            z1.setX( z1x) ; z1.setY(z1y); z2.setX(z2x) ; z2.setY(z2y) ;
            this.repaint();
        }


    } // END of the giga BoardPanel class

    public class Flasher extends Thread {
        byte c =0;
        Graphics g = BP.getGraphics().create() ;

        public void run(){
            BP.setZaria(true);
            while(true){
                if(!flash){
                    whidice[(dice[0] -1)<0?(byte) (Math.random() * 6):dice[0] -1].paintIcon(null,g,BP.z1x,BP.z1y);
                    whidice[(dice[1] -1)<0?(byte) (Math.random() * 6):dice[1] -1].paintIcon(null,g,BP.z2x,BP.z2y);
                    return ;
                }
                while(! stoprolling){;} //aston na rollarei prwta
                switch(c){
                case 1:
                try{sleep(600);}catch(InterruptedException e){}
                    whidice[(dice[0] -1)<0?(byte) (Math.random() * 6):dice[0] -1].paintIcon(null,g,BP.z1x,BP.z1y);
                    whidice[(dice[1] -1)<0?(byte) (Math.random() * 6):dice[1] -1].paintIcon(null,g,BP.z2x,BP.z2y);
                    c = 0;
                    break ;
                default :
                try{sleep(600);}catch(InterruptedException e){}
                    yeldice[(dice[0] -1)<0?(byte) (Math.random() * 6):dice[0] -1].paintIcon(null,g,BP.z1x,BP.z1y);
                    yeldice[(dice[1] -1)<0?(byte) (Math.random() * 6):dice[1] -1].paintIcon(null,g,BP.z2x,BP.z2y);
                    c = 1 ;
                }
            }
        }
    }
    public class Roller extends Thread {
        int x1,x2,y1,y2,factor ;
        short i =0;
        Graphics g = BP.getGraphics().create() ;

        public void run(){
            BP.setZaria(false);
            lala(krr,false);
            while(true){
                i++ ;
                factor = (Math.random() - 0.5)>0?1:-1  ;
                x1 =BP.z1x +  (int) ( (Math.random() * 35) * factor  ) ;
                x2 =BP.z2x +  (int) ( (Math.random() * 35) * -factor  ) ;
                y1 =BP.z1y +  (int) ( (Math.random() * 35) * factor  ) ;
                y2 =BP.z2y +  (int) ( (Math.random() * 35) *  -factor ) ;

                if(x1 > 270){
                    x1 -= (35* 2) ;
                } else if(x1 < 12){
                    x1 +=  (35* 2) ;
                }
                if(x2 > 270){
                    x2 -=(35*2) ;
                } else if(x2 < 12){
                    x2 += (35* 2) ;
                }
                if(y1 > 260){
                    y1 -=  (35* 2) ;
                } else if(y1 < 40){
                    y1 +=  (35* 2) ;
                }
                if(y2 > 260){
                    y2 -= (35 * 2);
                } else if(y2 < 40){
                    y2 +=  (35* 2) ;
                }
                whidice[(byte) (Math.random() * 6)].paintIcon(null,g,x1,y1);
                whidice[(byte) (Math.random() * 6)].paintIcon(null,g,x2,y2);
                if(i % 3 == 1){
                    BP.repaint() ;
                }
                if(i >1000 || stoprolling){
                    if(sound){
                        try{
                            krr.stop();
                        }catch(NullPointerException e){
                            System.err.println(e.getMessage() +": No Soundfile loaded");
                        }
                    }
                    BP.setZaria(true);
                    undo.setEnabled(true);
                    stoprolling = true ;
                    BP.repaint();
                    return ;
                }
                try{sleep(150 );}catch(InterruptedException e){}
            }
        }

    }



    public class ListUpdater extends Thread {
        boolean who=false, games=false, names=false ;
        boolean Pwho=false, Pgames=false, Pnames=false ;
        short counter =0;
        public void setWho(){
            Pwho = true;
        }
        public void setNames(){
            Pnames = true ;
        }
        public void setGames(){
            Pgames = true;
        }

        public void run(){

            while(connected){
                counter++;
                if(counter == 12){
                    System.gc() ;
                    Pgames=Pwho = true ;
                    if(onChan){
                        Pnames = true ;
                    }
                    counter = 0;
                }
                names = Pnames ;
                games = Pgames ;
                who = Pwho ;
                Pwho = Pgames = Pnames = false ;

                if(who){
                    twhoV.clear();
                    send("/who");
                    try{sleep(100);}catch(Exception e){} /*-- just giving some time because of output collitions --*/
                }
                if(games){
                    tgamesV.clear();
                    send("/games");
                    try{sleep(100);}catch(Exception e){} /*-- just giving some time because of output collitions --*/
                }
                if(names){
                    tnamesV.clear();
                    send("/names");
                }



                try{sleep(5000);}catch(Exception e){} /*-- just giving some time because of output collitions --*/
                int i,k ;

                if(who){
                    k = twhoV.getSize();
                    if(k==0){
                        try{sleep(5000);}catch(Exception e){}
                        k = twhoV.getSize();
                    }
                    whoV.setSize(k);
                    for(i = 0; i<k;i++){
                        whoV.set(i,new String( (String) twhoV.elementAt(i)));
                    }
                }
                if(games){
                    k= tgamesV.getSize();
                    gamesV.setSize(k);
                    for(i = 0; i<k;i++){
                        gamesV.set(i,new String( (String) tgamesV.elementAt(i)));
                    }
                }
                if(names){
                    k= tnamesV.getSize();
                    namesV.setSize(k);
                    for(i = 0; i<k;i++){
                        namesV.set(i,new String( (String) tnamesV.elementAt(i)));
                    }
                }
            }

        }
    }


    public class Inviter extends Thread {
        String name ;
        String points ;
        Inviter(String s, String pts){
            name = s ;
            points = pts ;
            if (points.length() > 3){
                points = points + " 1";
            }
        }
        public void run(){
            int res = JOptionPane.showConfirmDialog(glob,(name + " wants to play a " + points + " points game with you! Accept?"),"Challenge to a game",JOptionPane.YES_NO_OPTION,JOptionPane.PLAIN_MESSAGE);
            if(res == JOptionPane.YES_OPTION){
                send("/acc " + name);
            }else{
                send("/msg " + name + " Sorry... An other time...");
            }
        }
    }
    public class Taker extends Thread{
        byte hm ;
        Taker(byte i){
            hm = i ;
        }
        public void run(){
            int res = JOptionPane.showConfirmDialog(glob,(oponick + " wants to let  " + gN[gameType]  +"( "+ hm + " points)! Accept?"),"Giving up",JOptionPane.YES_NO_OPTION,JOptionPane.PLAIN_MESSAGE);
            if(res == JOptionPane.YES_OPTION){
                send("/take ");
            }
        }
    }
    public class Giver extends Thread{
        byte hm ;
        Giver(byte i){
            hm = i ;
        }
        public void run(){
            int res = JOptionPane.showConfirmDialog(glob,"Are you sure you want to give "+hm+" point?","Give?",JOptionPane.YES_NO_OPTION,JOptionPane.PLAIN_MESSAGE);
            if( res == JOptionPane.YES_OPTION){
                send("/give "+ hm);
            }


        }
    }
    public class Resigner extends Thread{
        public void run(){
            int res = JOptionPane.showConfirmDialog(glob,"Are you sure you want to resign?","Resign?",JOptionPane.YES_NO_OPTION,JOptionPane.PLAIN_MESSAGE);
            if( res == JOptionPane.YES_OPTION){
                send("/resign");
            }

        }

    }
    public class Namer extends Thread{
        public void run(){
            String s = (String) JOptionPane.showInputDialog(glob,"","Enter your name", JOptionPane.PLAIN_MESSAGE);
            if(s.length() >0){
                send("/name "+ s);
            }
        }
    }
    public class Helper extends Thread {
        byte w ;
        Helper(byte b){
            w = b ;
        }
        public void run(){
            if(w == 0){
                JOptionPane.showMessageDialog(glob,"Java client for the Greek Tavli Server.\nAuthor: Antonis Kanavouras","Tavli " +VERSION,JOptionPane.PLAIN_MESSAGE);
            }else{
                if(isApplet){
                    try{
                        getAppletContext().showDocument(new URL(getCodeBase() + "help.html"),"_blank");
                    }catch(MalformedURLException ue){}
                }else{
                    JOptionPane.showMessageDialog(glob,"Available at: http://www.exares.gr/help.html","Tavli " +VERSION,JOptionPane.PLAIN_MESSAGE);
                }

            }
        }
    }
    public class Privater extends Thread {
        public void run(){
            int res = JOptionPane.showConfirmDialog(glob,"Are you sure you want to set game private?","Prive?",JOptionPane.YES_NO_OPTION,JOptionPane.PLAIN_MESSAGE);
            if( res == JOptionPane.YES_OPTION){
                send("/prive");
            }
        }
    }
    /*--- END OF GLOBALS & CLASSES --*/

    public synchronized  void send(String s){
        while (sendlock) {
            try {
                wait();
            } catch (InterruptedException e) { }
        }
        sendlock = true;

        try{
            out.flush();
            out.print(s + "\r\n");
            out.flush();
        } catch(NullPointerException npe){
            System.out.println(npe.getMessage());
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        sendlock = false ;
        notifyAll();
    }

    public void sendMove(){
        StringBuffer buf = new StringBuffer(64);
        buf.append("/m ");
        for(byte i =0;i<donemoves -1;i++){
            buf.append( moves[i] + ",");
        }
        canmove = false ;
        buf.append(moves[donemoves - 1]);
        out.flush();
        try{Thread.sleep(100); }catch(InterruptedException ie){System.err.println(ie.getMessage());}
        send(buf.toString());
        //System.out.println(buf.toString());
    }

    public void roll(){
        if(player) {
            out.flush();
            try{Thread.sleep(100); }catch(InterruptedException ie){System.err.println(ie.getMessage());}
            send("/roll ");
            flash(false) ;
            stoprolling = false ;
            rolled = true ;
            loaded = -1 ;
        }
        roller = new Roller();
        roller.start();
    }

    public void flash(boolean b){
        flash = b;
        stoprolling = true ;
        if(b && player){
            flasher = new Flasher();
            flasher.start();
        }
    }

    public void lala(AudioClip boo,boolean loop ){
        if(!sound)
            return ;
        try{
            if(loop){
                boo.loop();
            }else{
                boo.play();
            }
        }catch(NullPointerException ne){
            System.err.println(ne.getMessage() +": No Soundfile loaded");
        }catch(ArrayIndexOutOfBoundsException ne){
            System.err.println(ne.getMessage());
        }

    }

    public void run(){
        String s;
        try{

            while (connected){
                if(! sock.isConnected()){
                    doDisconnection();
                }

                if(1 ==1 ){
                    s= in.readLine();

                    if(s.length() >0){
                        if(s.charAt(0) == '%'){
                            updateWho(s);
                            continue;
                        }
                        if(s.charAt(0) == '@'){
                            updateGames(s);
                            continue;
                        }
                        if(s.charAt(0) == '#'){
                            updateNames(s);
                            continue;
                        }
                        if(s.equals("*** There are not any games currently") ||s.equals("*** Nobody wants to start a game. Sorry...")){
                            continue ;
                        }
                        if(s.indexOf("***->")==0){
                            doInvitation(s);
                        }
                        if(s.indexOf("*-*") == 0 || s.indexOf("*+*") == 0){
                            try{
                                Lu.setNames() ;
                            }catch(NullPointerException np){}
                        }
                        if(s.indexOf("*=*")==0){
                            try{
                                Lu.setNames() ;
                                Lu.setWho();
                            }catch(NullPointerException np){}
                        }
                        if(s.charAt(0) == '*' || s.charAt(0) == '<'){
                            toIRC(s + "\n");
                            continue;
                        }
                        if(s.charAt(0) =='G'){
                            dogui(s.substring(2));
                        }
                    }
                }
            }
            return ;
        } catch(IOException e){
            if(connected){
                doDisconnection();
            }
        }
    }

    /*--- net input processing functions --*/
    public void updateWho(String s){
        twhoV.addElement(new String( s.substring(3)));
    }
    public void updateGames(String s){
        tgamesV.addElement(new String(s.substring(3)));
    }
    public void updateNames(String s){
        StringTokenizer tok = new StringTokenizer(s,":");
        tok.nextToken();
        tok.nextToken(" ");
        tnamesV.addElement(new String(tok.nextToken(" ")));
        while(tok.hasMoreTokens()){
            tnamesV.addElement(new String(tok.nextToken(" ")));
        }
    }

    public void doInvitation(String s){
        StringTokenizer tok = new StringTokenizer(s);
        String name ;
        String pts ;
        tok.nextToken();
        name = tok.nextToken();
        if ( name.equals("type:") ){
            return ;
        }
        tok.nextToken(); // invites
        tok.nextToken(); // you
        tok.nextToken(); // for
        tok.nextToken(); // a
        pts = tok.nextToken();

        lala(ding,false);
        Inviter inviter = new Inviter(name, pts);
        inviter.start();

    }

    public void doLabel(byte tclr){
        String bln = (tclr == BLACK?"<u>"+blackNick+"</u>":blackNick) ;
        String whn= (tclr == WHITE?"<u>"+whiteNick+"</u>":whiteNick) ;
        labGame.setText("<html>"+ "<font color=#0000aa>"+  bln+ "</font>" + "&nbsp; vs &nbsp;<font color=#ffffff>&nbsp;" + whn + "</font>&nbsp;"+ blackScor + "-" + whiteScor +"&nbsp;&nbsp;("+ gN[gameType] + ")</html>");

    }

    public void showmoves(byte moves){
        switch(moves){
        case 0 :
            labMoves.setText("<html><font color=#990000>  can not move...</font></html>");
            break ;
        case -2:
            labMoves.setText("<html><font color=#990000>" + (turncolor==WHITE?whiteNick:blackNick) + "&nbsp roll</font></html>");
            break ;
        case -1:
            labMoves.setText("");
            break ;
        default:
            labMoves.setText("<html><font color=#009900> >> " + moves +(moves==1? " move":" moves")+ "</font></html>");
        }
    }


    public void dogui(String s){
        String cmd ;
        StringTokenizer tok = new StringTokenizer(s);
        cmd = tok.nextToken();

        if (cmd.equals("NICK")){
            nick = tok.nextToken();
            labNick.setText(" " + nick+ "  |  ");
            return ;
        }

        if(cmd.equals("GM")){
            try{
                Lu.setGames();
                Lu.setWho();
            }catch(NullPointerException np){}
            return ;
        }

        if(cmd.equals("WHO")){
            try{
                Lu.setWho();
            }catch(NullPointerException np){}
            return ;
        }

        if(cmd.equals("NOCHAN")){
	    given = false ;
            onChan = player =turn=canmove=  false ;
            rolled = true ;
            try{ tim.cancel(); tim=null;}catch(Exception le){}
            blackScor = whiteScor = 0;
            playercolor = -1 ;
            labGame.setText("");
            labMoves.setText("");
            namesV.clear();
            doMenuJob();
            flash(false);
            flasher = null ;
            gameType = -1 ;
            BP.setZaria(false);
            BP.initGame(gameType,BLACK);
            BP.repaint();
            IRC.setText(prevIRCString);
            return ;
        }

        if(cmd.equals("CHAN")){
	    given = false ;
            onChan =rolled= true ;
            turn =player= false ;
            blackScor = whiteScor = 0;
            playercolor = -1 ;
            IRC.setText("");
            try{ tim.cancel(); tim=null;}catch(Exception le){}

            if(tok.nextToken().equals("W")){
                blackNick = tok.nextToken();
                blackScor =  (byte)Short.parseShort(tok.nextToken());
                whiteNick = tok.nextToken();
                whiteScor = (byte)Short.parseShort(tok.nextToken());
                dice[0] =  (byte)Short.parseShort(tok.nextToken());
                dice[1] = (byte)Short.parseShort(tok.nextToken());
                turncolor = (byte)Short.parseShort(tok.nextToken());
                BP.createBoardMap(player,playercolor);
                BP.initGame((byte) -2,playercolor);
                gameType =(byte)Short.parseShort(tok.nextToken());
                try{
                    Lu.setNames();
                }catch(NullPointerException np){}
            } else { /*-- starting a game --*/
                player = true ;
                playercolor = (byte)Short.parseShort(tok.nextToken());
                oponick = tok.nextToken();
                if(playercolor == BLACK){
                    blackNick = nick ;
                    whiteNick = oponick ;
                }else{
                    blackNick = oponick ;
                    whiteNick = nick ;
                }
                dice[0] =  (byte)Short.parseShort(tok.nextToken());
                dice[1] = (byte)Short.parseShort(tok.nextToken());
                turncolor = dice[0]>dice[1]?BLACK:WHITE ;
                turn = (playercolor == turncolor?true:false) ;
                showmoves((byte)-2);
                gameType = (byte)Short.parseShort(tok.nextToken());
                BP.createBoardMap(player,playercolor);
                BP.initGame(gameType,playercolor);
                try{
                    Lu.setNames();
                    Lu.setWho();
                }catch(NullPointerException np){}
                toIRC("*** Time limit: 3 min per move\n");
            }

            BP.setZaria(true);
            if(turn){
                rolled = false ;
                flash(true);
            }

            doLabel(turncolor);
            doMenuJob();
            repaint();
            return ;
        }

        if(cmd.equals("ROLL")){

            exmoves = 0 ;
            donemoves = 0;
	    given = false ;

            byte t = (byte)Short.parseShort(tok.nextToken());
            dice[0] =dice[1] =dice[2] =dice[3] =0 ;
            dice[0] =(byte)Short.parseShort(tok.nextToken());
            dice[1] =(byte)Short.parseShort(tok.nextToken());
            exmoves = (byte)Short.parseShort(tok.nextToken());
            exmovesback = exmoves ;


            if(t != playercolor){
                roller = new Roller();
                stoprolling = false ;
                roller.start();
                try{ Thread.sleep(900) ;}catch(InterruptedException ie){System.err.println(ie.getMessage());}
                stoprolling = true ;
            }	else {
                /* let him roll for a while */
                try{ Thread.sleep(600) ;}catch(InterruptedException ie){System.err.println(ie.getMessage());}
                stoprolling = true ;
                canmove = true ;
            }

            if(dice[0] == dice[1]){
                dice[2] =dice[3] = dice[0] ;
            }else if(dice[1] > dice[0]){/*-- for mazema --*/
                byte z = dice[1] ;
                dice[1] = dice[0] ;
                dice[0] = z ;
            }

            for(byte i =0; i<4;i++){
                backupdice[i] =dice[i] ;
            }

            if (exmoves == 0){
                turncolor =( t == BLACK?WHITE:BLACK);
                if(turncolor != playercolor){
                    turn =true ;
                    rolled = false ;
                    flash(true);
                    if(player){
                        tim = new  java.util.Timer();
                        task=null;
                        task = new Tasky();
                        tim.schedule(task, 180000);
                    }
                }else {
                    turn =false ;
                    rolled =true ;
                    try{ tim.cancel(); tim=null;}catch(Exception le){}
                }
            }
            BP.setZaria(true);
            doLabel(turncolor);
            showmoves(exmoves);
            return ;
        }

        if(cmd.equals("INIT")){
	    given = false ;
            gameType = (byte)Short.parseShort(tok.nextToken());
            blackScor =  (byte)Short.parseShort(tok.nextToken());
            whiteScor =  (byte)Short.parseShort(tok.nextToken());
            turncolor =   (byte)Short.parseShort(tok.nextToken());
            doLabel(turncolor);
            try{ tim.cancel() ;} catch(Exception el){}
            showmoves((byte)-1);
            if(player && turncolor == playercolor){
                turn = true ;
                rolled = false ;
            } else {
                turn = false ;
                rolled = true ;
            }
            BP.createBoardMap(player,playercolor);
            BP.initGame(gameType,playercolor);
            BP.setZaria(true);
            for(byte i=0;i<4;i++){
                dice[i] = backupdice[i] = 0 ;
            }
            if(turn){
                flash(true);
                tim = new  java.util.Timer();
                task=null;
                task = new Tasky();
                tim.schedule(task, 180000);
            }
            BP.repaint();
            if(blackScor >0 || whiteScor >0){
                lala(hurrah,false);
            }
            return ;
        }

        if(cmd.equals("REJ")){
	    given = false ;
            turn = true ;
            for(byte i =0;i<4;i++){
                dice[i] = backupdice[i] ;
            }
            BP.restoreOrig();
            BP.cpOrigToTmp();
            donemoves = 0;
            exmoves = exmovesback ;
            undo.setEnabled(true);
            BP.repaint();
            canmove = true ;
            return ;
        }

        if(cmd.equals("POS")){
            StringTokenizer t = new StringTokenizer(new String(tok.nextToken("E")),"#");
            byte i = 0 ;
            while(t.hasMoreTokens()){
                tok = new StringTokenizer(t.nextToken());
                try{
                    i=(byte)Short.parseShort(tok.nextToken());
                    BP.tmpcol[i].bpop =(byte)Short.parseShort(tok.nextToken());
                    BP.tmpcol[i].wpop =(byte)Short.parseShort(tok.nextToken());
                    BP.tmpcol[i].top =(byte)Short.parseShort(tok.nextToken());
                }catch(NumberFormatException nfe){
                    System.err.println("Something wrong: "+ nfe.getMessage());
                }
            }
            BP.createBoardMap(player,playercolor);
            BP.cpTmpToOrig();
            BP.repaint();
            return ;
        }

        if(cmd.equals("MOVE")){
            byte c=(byte)Short.parseShort(tok.nextToken());
            String mv = new String(tok.nextToken());
            turncolor =(c==BLACK?WHITE:BLACK);
            doLabel(turncolor);
            if( c == playercolor){
                turn = false ;
                showmoves((byte)-1);
                BP.equcols() ;
                BP.setZaria(true);
                BP.repaint();
                return ;
            }

            tok =  new StringTokenizer(mv,",");
            while(tok.hasMoreTokens()){
                byte start ;
                StringTokenizer t = new StringTokenizer(new String(tok.nextToken()),"/");
                try{
                    start = (byte)Short.parseShort(t.nextToken());
                }catch (NumberFormatException nf){
                    if(c == BLACK){
                        start = 0;
                    }else{
                        start = 25 ;
                    }
                }

                byte die = (byte)Short.parseShort(t.nextToken());
                BP.domove(BP.mycol,start,die,c,gameType);
                byte r = (byte)((Math.random()) * tsaf.length) ;
                lala(tsaf[r],false);
                BP.repaint();
                try{ Thread.sleep(400) ;}catch(InterruptedException ie){System.err.println(ie.getMessage());}
            }

            BP.equcols();
            turn = player ;
            showmoves((byte)-2);
            BP.setZaria(true);
            BP.repaint();
            if(player){
                rolled = false ;
                flash(true);
                tim = new  java.util.Timer();
                task=null;
                task = new Tasky();
                tim.schedule(task, 180000);
            }
            return ;
        }

        if(cmd.equals("WIN")){
	    given = false ;
            lala(hurrah,false);
            onChan = player =turn=canmove=  false ;
            rolled = true ;
            try{ tim.cancel(); tim=null;}catch(Exception le){}
            blackScor = whiteScor = 0;
            playercolor = -1 ;
            labGame.setText("");
            labMoves.setText("");
            namesV.clear();
            doMenuJob();
            flash(false);
            flasher = null ;
            gameType = -1 ;
            BP.setZaria(false);
            BP.initGame(gameType,BLACK);
            BP.repaint();
            return ;
        }

        if(cmd.equals("GIVE")){
            byte hm = (byte)Short.parseShort(tok.nextToken());
            Taker taker = new Taker(hm);
            taker.start();
        }

    }// end of dogui()



    public void doMenuJob(){
        if(connected){
            stateMenu.setEnabled(true);
            login.setEnabled(true);
            nameM.setEnabled(true);
        }else{
            stateMenu.setEnabled(false);
            login.setEnabled(false);
            nameM.setEnabled(false);
        }

        if(onChan){
            gameMenu.setEnabled(true);
        }else{
            gameMenu.setEnabled(false);
        }

        if(player){
            part.setEnabled(false);
            resign.setEnabled(true);
            giveS.setEnabled(true);
            giveD.setEnabled(true);
            kick.setEnabled(true);
            ban.setEnabled(true);
            prive.setEnabled(true);
            quiet.setEnabled(true);
            login.setEnabled(false);
        }else{
            part.setEnabled(true);
            resign.setEnabled(false);
            giveS.setEnabled(false);
            giveD.setEnabled(false);
            kick.setEnabled(false);
            ban.setEnabled(false);
            prive.setEnabled(false);
            quiet.setEnabled(false);
            login.setEnabled(true);
        }
    }


    public void showLogin (int x, int y){
        frLogin = new loginWin ("Login");
        frLogin.setSize (400, 200);
        frLogin.setLocation (new Point (x, y));
        frLogin.setResizable (false);
        frLogin.pack ();
        frLogin.show ();
        frLogin.toFront();
    }

    public void toIRC(String s){
        IRC.append(s);
        IRC.setCaretPosition(IRC.getText().length());
        prevIRCString =  s ;
    }



    public void actionPerformed(ActionEvent a){
        Object o ;
        o = a.getSource();
        if(o == inIRC){
            if(inIRC.getText().length() == 0){
                return ;
            }
            if(! connected){
                toIRC("Not connected\n");
            }   else {
                if(! sock.isConnected()){
                    doDisconnection();
                    toIRC("Not connected\n");
                    inIRC.setText("");
                    inIRC.grabFocus();
                    return;
                }
                send(inIRC.getText());
            }

            inIRC.setText("");
            inIRC.grabFocus();
        }
    }


    public void doConnection(){
        if(connected){
            toIRC("You are allready connected\n");
            return ;
        }
        IRC.append("Connecting....\n");
        try{
            sock = new Socket(server,port);
            sock.setTcpNoDelay(true);
            in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            out  = new PrintWriter(sock.getOutputStream(), true);
        }
        catch(UnknownHostException e){
            toIRC(server + " is an unknown host. Sorry...\n");
            doMenuJob();
            return ;
        }
        catch(IOException e ){
            toIRC("Can't connect to " + server +"\n");
            doMenuJob();
            return;
        }

        toIRC("Connected to " + server + "\n\n");
        connected =true ;
        doMenuJob();
        NetReader = new Thread (this);
        NetReader.start();
        send("/gui");
        Lu = new ListUpdater();
        try{
            Lu.setGames();
            Lu.setWho();
            Lu.start();
        }catch(NullPointerException np){}
        showLogin (130,200);
    }

    public void doDisconnection(){
        if(! connected){
            toIRC("Not connected\n");
            return ;
        }

        connected = false ;
        onChan = player = turn = rolled = canmove = false ;
        blackScor =  whiteScor = 0 ;
        gameType = -1 ;
        playercolor =-1 ;
        doMenuJob();
        labNick.setText(" Not connected ");
        labGame.setText("");
        labMoves.setText("");
        BP.initGame(gameType,playercolor);
        try{
            if(in != null){
                in.close();
                in = null ;
            }
            if(out != null){
                out.close();
                out = null;
            }
            if(sock != null){
                sock.close();
                sock = null ;
            }
            toIRC("Disconnected from " + server + "\n");
            whoV.clear();
            gamesV.clear();
            namesV.clear();
            twhoV.clear();
            tnamesV.clear();
            tgamesV.clear();
            flash(false);
            flasher = null ;
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void init () {

        Box sPan = Box.createVerticalBox();
        JPanel chat = new JPanel() ;
        JLabel chatlab = new JLabel("Chat!",JLabel.CENTER); ;
        chat.setLayout(new BorderLayout());

        nCur = new Cursor(Cursor.DEFAULT_CURSOR);
        hCur = new Cursor(Cursor.HAND_CURSOR);
        menu = new JMenuBar ();
        connection = new JMenu ("User");
        login = new JMenuItem ("login");
        nameM = new JMenuItem("name");
        connect = new JMenuItem("Connect");
        disconnect = new JMenuItem("Disconnect");
        login.addActionListener (new MenuItemHandler ());
        nameM.addActionListener (new MenuItemHandler ());
        connect.addActionListener (new MenuItemHandler ());
        disconnect.addActionListener (new MenuItemHandler ());
        connection.add (login);
        connection.add(nameM);
        connection.add (connect);
        connection.add (disconnect);

        stateMenu = new JMenu("State");
        free = new JMenuItem("free");
        silence = new JMenuItem("silence");
        free.addActionListener (new MenuItemHandler ());
        silence.addActionListener (new MenuItemHandler ());
        stateMenu.add(free);
        stateMenu.add(silence);

        gameMenu = new JMenu("Game");
        part = new JMenuItem("part");
        resign = new JMenuItem("resign");
        giveS = new JMenuItem("give 1 point");
        giveD = new JMenuItem("give 2 points");
        prive = new JMenuItem("prive");
        quiet = new JMenuItem("quiet");
        part.addActionListener (new MenuItemHandler ());
        resign.addActionListener (new MenuItemHandler ());
        giveS.addActionListener (new MenuItemHandler ());
        giveD.addActionListener (new MenuItemHandler ());
        prive.addActionListener (new MenuItemHandler ());
        quiet.addActionListener (new MenuItemHandler ());
        gameMenu.add(part);
        gameMenu.add(resign);
        resign.setEnabled(false);
        gameMenu.add(giveS);
        giveS.setEnabled(false);
        gameMenu.add(giveD);
        giveD.setEnabled(false);
        gameMenu.add(prive);
        prive.setEnabled(false);
        gameMenu.add(quiet);
        quiet.setEnabled(false);
        gameMenu.setEnabled(false);

        sndMenu = new JMenu("sound");
        ButtonGroup sndgrp = new ButtonGroup() ;
        sndon = new JRadioButtonMenuItem("on",true);
        sndoff = new JRadioButtonMenuItem("off",false);
        sndon.addItemListener(new MenuItemHandler());
        sndoff.addItemListener(new MenuItemHandler());
        sndgrp.add(sndon);
        sndgrp.add(sndoff);
        sndMenu.add(sndon);
        sndMenu.add(sndoff);

        helpm = new JMenu("help");
        help = new JMenuItem("help");
        about = new JMenuItem("about");
        help.addActionListener(new MenuItemHandler());
        about.addActionListener(new MenuItemHandler());
        helpm.add(help);
        helpm.add(about);
        menu.add (connection);
        menu.add(stateMenu);
        menu.add(gameMenu);
        menu.add(sndMenu);
        menu.add(helpm);
        setJMenuBar (menu);
        /*--- end of menu --*/

        container = getContentPane ();
        whoV= new DefaultListModel();
        namesV= new DefaultListModel();
        gamesV= new DefaultListModel();
        twhoV= new DefaultListModel();
        tnamesV= new DefaultListModel();
        tgamesV= new DefaultListModel();
        who = new JList(whoV);
        who.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        who.addMouseListener(new MouseAdapter(){
                                 public void mousePressed(MouseEvent e){
                                     cistrig(e);
                                 }
                                 public void mouseReleased(MouseEvent e){
                                     cistrig(e);
                                 }
                                 private void cistrig(MouseEvent e){
                                     if(e.isPopupTrigger() && who.getSelectedIndex() != -1){
                                         whoPop.show(e.getComponent(),e.getX(),e.getY());
                                     }
                                 }
                             }
                            );
        names = new JList(namesV);
        names.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        games = new JList(gamesV);
        names.addMouseListener(new MouseAdapter(){
                                   public void mousePressed(MouseEvent e){
                                       cistrig(e);
                                   }
                                   public void mouseReleased(MouseEvent e){
                                       cistrig(e);
                                   }
                                   private void cistrig(MouseEvent e){
                                       if(e.isPopupTrigger() && names.getSelectedIndex() != -1){
                                           namesPop.show(e.getComponent(),e.getX(),e.getY());
                                       }
                                   }
                               }
                              );
        games.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        games.addMouseListener(new MouseAdapter(){
                                   public void mousePressed(MouseEvent e){
                                       cistrig(e);
                                   }
                                   public void mouseReleased(MouseEvent e){
                                       cistrig(e);
                                   }
                                   private void cistrig(MouseEvent e){
                                       if(e.isPopupTrigger() && games.getSelectedIndex() != -1){
                                           gamesPop.show(e.getComponent(),e.getX(),e.getY());
                                       }
                                   }
                               }
                              );
        tabs = new JTabbedPane(JTabbedPane.TOP,JTabbedPane.SCROLL_TAB_LAYOUT);
        tabs.add("who",new JScrollPane(who));
        tabs.add("here",new JScrollPane(names));
        tabs.add("games",new JScrollPane(games));
        tabs.setSelectedIndex(0);

        whoPop = new JPopupMenu();
        wmsg = new JMenuItem("msg");
        wignore = new JMenuItem("ignore");
        wwhois = new JMenuItem("whois");
        invite = new JMenu("invite");
        point1 = new JMenu("1 point");
        invite3 = new JMenuItem("3 points");
        invite5 = new JMenuItem("5 points");
        invite7 = new JMenuItem("7 points");
        portes  = new JMenuItem("Portes");
        plakwto = new JMenuItem("Plakwto");
        feyga   = new JMenuItem("Feyga");

        invite3.addActionListener(new MenuItemHandler());
        invite5.addActionListener(new MenuItemHandler());
        invite7.addActionListener(new MenuItemHandler());
        portes.addActionListener(new MenuItemHandler());
        plakwto.addActionListener(new MenuItemHandler());
        feyga.addActionListener(new MenuItemHandler());
        cancelinv = new JMenuItem("cancel invitation");
        wmsg.addActionListener(new MenuItemHandler());
        wignore.addActionListener(new MenuItemHandler());
        wwhois.addActionListener(new MenuItemHandler());
        point1.add(portes);
        point1.add(plakwto);
        point1.add(feyga);
        invite.add(point1);
        invite.add(invite3);
        invite.add(invite5);
        invite.add(invite7);
        cancelinv.addActionListener(new MenuItemHandler());
        whoPop.add(wmsg);
        whoPop.add(wwhois);
        whoPop.add(invite);
        whoPop.add(cancelinv);
        whoPop.add(wignore);
        gamesPop = new JPopupMenu();
        watch = new JMenuItem("watch");
        watch.addActionListener(new MenuItemHandler());
        gamesPop.add(watch);
        namesPop = new JPopupMenu();
        nmsg = new JMenuItem("msg");
        nwhois = new JMenuItem("whois");
        kick = new JMenuItem("kick");
        ban = new JMenuItem("ban");
        nmsg.addActionListener(new MenuItemHandler());
        nignore = new JMenuItem("ignore");
        nwhois.addActionListener(new MenuItemHandler());
        kick.addActionListener(new MenuItemHandler());
        ban.addActionListener(new MenuItemHandler());
        nignore.addActionListener(new MenuItemHandler());
        namesPop.add(nmsg);
        namesPop.add(nwhois);
        namesPop.add(kick);
        namesPop.add(ban);
        namesPop.add(nignore);

        boardPop = new JPopupMenu();
        undo = new JMenuItem("undo");
        undo.addActionListener(new MenuItemHandler());
        boardPop.add(undo);

        labNick = new JLabel(" Not connected ");
        labGame = new JLabel("");
        labMoves = new JLabel("");
        glob = new JPanel ();
        glob.setLayout (new BorderLayout ());
        glob.setBorder (new LineBorder (Color.gray, 1));

        BP = new BoardPanel ();

        /* Ftiaxnei ena map ap' opou antlountai coordinates kai metrics
         * gia to drawing ths tavlieras 
         */
        map = new Map[28];
        for(int i =0;i<28;i++){
            map[i]=new Map();
        }
        map[0].x = 12  ; map[0].y =10 ; map[0].vstep = 7; /*Xwros Mazematos */
        /* Anw aristero panel */
        map[1].x =lpX  ; map[1].y =10 ; map[1].vstep =vStep ;
        map[2].x =lpX  +(1* 26) ; map[2].y =10 ; map[2].vstep =vStep ;
        map[3].x =lpX +(2* 26) ; map[3].y =10 ; map[3].vstep =vStep ;
        map[4].x =lpX +(3* 26) ; map[4].y =10 ; map[4].vstep =vStep ;
        map[5].x =lpX +(4* 26) ; map[5].y =10 ; map[5].vstep =vStep ;
        map[6].x =lpX +(5* 26) ; map[6].y =10 ; map[6].vstep =vStep ;
        /* Anw dexio panel */
        map[7].x =rpX  ; map[7].y =10 ; map[7].vstep =vStep ;
        map[8].x = rpX +(1*26) ; map[8].y =10 ; map[8].vstep =vStep ;
        map[9].x = rpX +(2*26)  ; map[9].y =10 ; map[9].vstep =vStep ;
        map[10].x =  rpX +(3*26) ; map[10].y =10 ; map[10].vstep =vStep ;
        map[11].x =  rpX +(4*26) ; map[11].y =10 ; map[11].vstep =vStep ;
        map[12].x =  rpX +(5*26) ; map[12].y =10 ; map[12].vstep =vStep ;
        /* Katw dexio panel */
        map[13].x =rpX +(5*26) ; map[13].y =275 ; map[13].vstep =vStep * -1 ;
        map[14].x = rpX +(4*26) ; map[14].y =275 ; map[14].vstep =vStep * -1 ;
        map[15].x = rpX +(3*26)  ; map[15].y =275 ; map[15].vstep =vStep * -1 ;
        map[16].x =  rpX +(2*26) ; map[16].y =275 ; map[16].vstep =vStep * -1 ;
        map[17].x =  rpX +(1*26) ; map[17].y =275 ; map[17].vstep =vStep * -1 ;
        map[18].x =  rpX  ; map[18].y =275 ; map[18].vstep =vStep * -1 ;
        /* Katw aristero panel */
        map[19].x =lpX + (5*26) ; map[19].y =275 ; map[19].vstep =vStep * -1 ;
        map[20].x = lpX +(4*26) ; map[20].y =275 ; map[20].vstep =vStep * -1 ;
        map[21].x = lpX +(3*26)  ; map[21].y =275 ; map[21].vstep =vStep * -1 ;
        map[22].x =  lpX +(2*26) ; map[22].y =275 ; map[22].vstep =vStep * -1 ;
        map[vStep].x =  lpX +(1*26) ; map[vStep].y =275 ; map[vStep].vstep =vStep * -1 ;
        map[24].x =  lpX  ; map[24].y =275 ; map[24].vstep =vStep * -1 ;
        /* Xwros mazematos */
        map[25].x =  12  ; map[25].y =292 ; map[25].vstep = -7 ;
        /* Anw takos */
        map[26].x =  200  ; map[26].y =10 ; map[26].vstep = vStep ;
        /* Katw takos */
        map[27].x =  200  ; map[27].y =275 ; map[27].vstep = vStep * -1 ;

        /*------ Click areas coordinates --*/
        for (int i =0;i<26;i++){
            map[i].alx = map[i].x ;
            map[i].arx = map[i].x + pouliWidth ;
            if(map[i].vstep <0){
                map[i].ady = 298 ;
                map[i].auy = 298 - (5 * pouliWidth) ;
            }else {
                map[i].auy = 10 ;
                map[i].ady = 10 + (5 * pouliWidth) ;
            }
        }

        /*-- END Dokimastiko map --*/
        yeldice = new Zari[6] ;
        whidice = new Zari[6];

        for( byte i = 0;i<6;i++){
            if(isApplet){
                yeldice[i] = new Zari( getImage(getDocumentBase(),"images/y"+ ( i+1) +".gif"));
                whidice[i] = new Zari( getImage(getDocumentBase(),"images/w"+ (i+1) +".gif"));
            }else{
                yeldice[i] = new Zari( "images/y"+ (i+1) +".gif");
                whidice[i] = new Zari( "images/w"+(i+1) +".gif");
            }
        }

        tsaf = new AudioClip[3];
        if(isApplet){

            krr = getAudioClip(getDocumentBase(),"snd/krr.au");
            ding =getAudioClip(getDocumentBase(),"snd/ding.au");
            hurrah = getAudioClip(getDocumentBase(),"snd/hurrah.au");
            for(byte i =0; i<tsaf.length ; i++){
                tsaf[i] = getAudioClip(getDocumentBase(),"snd/tsaf"+ i + ".au");
            }

        }else{
            try{
                URL base =new URL( "file:" + System.getProperty("user.dir") + "/") ;
                krr = Applet.newAudioClip(new URL(base,"snd/krr.au"));
                ding = Applet.newAudioClip(new URL(base,"snd/ding.au"));
                hurrah = Applet.newAudioClip(new URL(base,"snd/hurrah.au"));
                for(byte i =0; i<tsaf.length ; i++){
                    tsaf[i] = Applet.newAudioClip(new URL(base,"snd/tsaf" + i +".au"));
                }
            } catch (MalformedURLException e){
                System.err.println(e.getMessage());
            }

        }

        moves = new String[4] ;

        Box nBox =  Box.createHorizontalBox();
        nBox.add(labNick);
        nBox.add(labGame);
        nBox.add(labMoves);
        nBox.setBackground(new Color(150,100,80));
        labNick.setBackground(new Color(150,100,80));
        labGame.setBackground(new Color(150,100,80));
        labMoves.setBackground(new Color(150,100,80));
        glob.add(nBox,BorderLayout.NORTH);
        glob.add(BP, BorderLayout.WEST);
        glob.add(tabs,BorderLayout.CENTER);
        IRC = new JTextArea(6,40);
        IRC.setWrapStyleWord(true);
        IRC.setLineWrap(true);
        IRC.setEditable(false);
        IRC.setFont(new Font("Serif",Font.PLAIN,14));
        inIRC = new JTextField(45);
        inIRC.addActionListener(this);
        sPan.add(new JScrollPane(IRC));
        inIRC.setBorder(new LineBorder(new Color(110,110,110),2));
        chat.add(chatlab,BorderLayout.CENTER);
        chat.add(inIRC,BorderLayout.EAST);
        sPan.add(chat);
        glob.add(sPan,BorderLayout.SOUTH);
        glob.setBorder(new LineBorder(new Color(110,110,110),1));

        container.add(glob);
        inIRC.grabFocus();
        doConnection();

        /* Initializing a board  */
        BP.createBoardMap(player,BLACK);
        gameType = -1 ;
        BP.initGame(gameType,BLACK);

    }

    /*=== This AApplet can run as an Application as well! ==*/
    public static void main(String[] args){
        int width = 550;
        int height = 510;
        JFrame appWin = new JFrame("Tavli");
        JApplet appletTavli = new tavli(false) ;
        appletTavli.setSize(width,height);
        appWin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        appletTavli.init();
        appWin.getContentPane().add(appletTavli);
        appWin.setSize(width,height);
        appWin.pack();
        appWin.show();
    }

    tavli(boolean isApplet){
        super();
        this.isApplet = false ;
    }

    public tavli(){
        super();
    }

    public void stop(){
        doDisconnection();
    }
    /*==== The final bracket ==*/
}
