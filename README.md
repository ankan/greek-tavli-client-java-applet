# README #
Disclaimer: I "learned" to program in Java bcs it was the easiest way to build a GUI client for the Greek Tavli Server. The GTS is a server written in ANSI C that allows people to connect and play Greek Tavli, a Greek version of backgammon, having different rules from original backgammon and two more similar ways to play in the same terrain.

The truth is that I never learnd to programm in Java, there is more procedural than OO logic in this project. There is a huge main() function and all the classes are nested in it.
In a Java Learning Book it can be used as a good Appendix  named "HOW NOT TO WRITE  JAVA" :-)

### How do I get set up? ###

Just compile it in Java SDK. It was written when Java Applets had not so strong security checks. Under the Oracle concept it needs at last a valid commercial security certificate (e.g. from Verisign). But it can be run as an application as well :-)

Antonis Kanavouras